package com.edsonjr.introducaopix

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.viewpager2.widget.ViewPager2
import com.edsonjr.introducaopix.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private val TAG = "[MainActivity]"
    private var adapter: IntroducaoPixViewPagerAdapter? = null

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //configurando o viewpager
        configureViewPager()

        //configurando os eventos de clicks dos botoes
        binding.buttonNextPagerSlide.setOnClickListener {
            binding.viewPagerIntroPix.setCurrentItem(1)
        }


        binding.buttonExitIntroPix.setOnClickListener {
            //TODO - REALIZAR CHAMADA DO PROXIMO FRAGMENT/ACTIVITY PARA LISTAGEM DE CHAVES PIX
            Log.d(TAG,"saindo do fluxo de introducao as chaves pix")
        }

    }

    /**
     * Este metodo configura a viewPager de introducao ao pix, bem como trabalha com os estados
     * da mesma.
     *
     * @author Edson Jr
     * */
    private fun configureViewPager() {
        val resTexts = resources.getStringArray(R.array.intro_pix_viewPager_texts)
        adapter = IntroducaoPixViewPagerAdapter(resTexts!!)
        binding.viewPagerIntroPix.adapter = adapter
        TabLayoutMediator(binding.dotsTabLayout,binding.viewPagerIntroPix){ tab, position-> }.attach()


        //configuracao do callback do pagerview
        binding.viewPagerIntroPix.registerOnPageChangeCallback(object:
            ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                Log.d(TAG, "position: $position")
                when(position){
                    0 -> { changeButtonToNextPage() }
                    1 -> { chageButtonToExitPixIntro() }
                }
            }
        })
    }




    /**
     * Este metodo serve para habilitar o botao de sair do fluxo de introducao ao pix.
     * Ele e mostrado quando o usuario esta na segunda pagina da viewPager de introducao ao pix
     * @author Edson Jr
     * */
    private fun chageButtonToExitPixIntro() {
        binding.buttonNextPagerSlide.visibility = View.GONE
        binding.buttonExitIntroPix.visibility = View.VISIBLE
    }



    /**
     * Este metodo serve para chamar a proxima pagina da viewPager de introducao ao pix, mostrando
     * o botao para que o usuario possa ir para o fluxo de listagem de chaves pix
     * @author Edson Jr
     * */
    private fun changeButtonToNextPage() {
        binding.buttonNextPagerSlide.visibility = View.VISIBLE
        binding.buttonExitIntroPix.visibility = View.GONE
    }

}