package com.edsonjr.introducaopix

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class IntroducaoPixViewPagerAdapter(val textList: Array<String>):
    RecyclerView.Adapter<IntroducaoPixViewPagerAdapter.IntroPixViewHolder>() {

    inner class IntroPixViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val text = itemView.findViewById<TextView>(R.id.textView_intro_pix_descritpion)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroPixViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pix_introducao_pager_layout,parent,false)
        return IntroPixViewHolder(view)
    }



    override fun onBindViewHolder(holder: IntroPixViewHolder, position: Int) {
        holder.text.text = textList?.get(position)!!
    }


    override fun getItemCount(): Int = textList?.size!!
}